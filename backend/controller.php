<?php
require 'dbconnection.php';

header('Access-Control-Allow-Origin: *');

$singleton = DBConnection::getInstance();
$dbconn = $singleton->getConnection();

$method = $_SERVER['REQUEST_METHOD'];
$requestData = file_get_contents("php://input");
$requestDataJson = json_decode($requestData, true);

$requestResponse = array();

if ($method == 'POST') {
    include 'login.php';
} elseif ($method == 'GET') {
    // Method is GET
} elseif ($method == 'PUT') {
    // Method is PUT

    include 'profile.php';
} elseif ($method == 'DELETE') {
    // Method is DELETE

} else {
    http_response_code(400);
    $requestResponse['error'] = "Method not found!";
}

echo json_encode($requestResponse);
?>