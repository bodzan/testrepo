<?php
  $requestDataJson['passwordhash'] = hash('sha3-512', $requestDataJson['password']);

  if ($requestDataJson['task'] == "register") {
    $requestDataJson['type'] = "user";
    $queryString = 'INSERT INTO `registeredusers`(`username`, `password`, `firstname`, `lastname`, `type`) VALUES (' .
      "'" . $requestDataJson['username'] . "'" . ", " .
      "'" . $requestDataJson['passwordhash'] . "'" . ", " .
      "'" . $requestDataJson['firstname'] . "'" . ", " .
      "'" . $requestDataJson['lastname'] . "'" . ", " .
      "'" . $requestDataJson['type'] . "'" . ");";
    $registerQuery = $dbconn->query($queryString);
    if (!$registerQuery) {
      http_response_code(400);
      $requestResponse['error'] = "Query not valid!";
    } else {
      $insertedRowID = $dbconn->query("SELECT `id` FROM `registeredusers` ORDER BY `id` DESC LIMIT 1;");
      $lastInsertedUserID = $insertedRowID->fetch_assoc();
      $requestResponse['id'] = $lastInsertedUserID['id'];
      $requestResponse['firstname'] = $requestDataJson['firstname'];
      $requestResponse['lastname'] = $requestDataJson['lastname'];
      $requestResponse['username'] = $requestDataJson['username'];
      $requestResponse['type'] = $requestDataJson['type'];
    }
  } elseif ($requestDataJson['task'] == "login") {
    $queryString = 'SELECT * FROM `registeredusers` WHERE `username`=' .
      "'" . $requestDataJson['username'] . "'" .
      " AND `password`=" . "'" . $requestDataJson['passwordhash'] . "'" . ";";
    $loginQuery = $dbconn->query($queryString);
    if ($dbconn->affected_rows == 0) {
      http_response_code(400);
      $requestResponse['error'] = "Invalid username or password!";
    } else {
      $userData = $loginQuery->fetch_assoc();
      $requestResponse['id'] = $userData['id'];
      $requestResponse['firstname'] = $userData['firstname'];
      $requestResponse['lastname'] = $userData['lastname'];
      $requestResponse['username'] = $userData['username'];
      $requestResponse['type'] = $userData['type'];
    }
  }
?>