window.addEventListener("DOMContentLoaded", myAdminFunction);

function myAdminFunction() {
  let httpRequest = new XMLHttpRequest();
  httpRequest.onload = () => {
    if (httpRequest.status == 200) {
      let requestResponse = JSON.parse(httpRequest.response);

      let colonId = createElement("td");
      colonId.innerHTML = sessionStorage.getItem("userID", requestResponse.id);
      let colonUser = createElement("td");
      colonUser.innerHTML = sessionStorage.getItem(
        "username",
        requestResponse.username
      );
      let colonName = createElement("td");
      colonName.innerHTML = sessionStorage.getItem(
        "firstname",
        requestResponse.firstname
      );
      let colonLastname = createElement("td");
      colonLastname.innerHTML = sessionStorage.getItem(
        "lastname",
        requestResponse.lastname
      );
      let colonType = createElement("td");
      colonType.innerHTML = sessionStorage.getItem(
        "type",
        requestResponse.type
      );
    }
    httpRequest.open(
      "GET",
      "http://localhost/maliprojekat/testrepo/backend/controller.php",
      true
    );

    httpRequest.send(JSON.stringify(requestData));
  };

  let edit = document.createElement("button");
  edit.innerHTML = "Edit";
  let erase = document.createElement("button");
  erase.innerHTML = "Delete";

  let div = document.getElementById("adminProfile");
  let table = document.createElement("table");
  let row = createElement("tr");
  // row.appendChild(requestResponse.users);
  row.appendChild(colonId);
  row.appendChild(colonUser);
  row.appendChild(colonName);
  row.appendChild(colonLastname);
  row.appendChild(colonType);
  table.appendChild(row);
  row.appendChild(edit);
  row.appendChild(erase);
  div.appendChild(table);

  edit.onclick = function() {
    changeUserProfile(requestResponse);
  };
  erase.onclick = function() {
    deleteUserProfile(this, requestResponse);
  };
}

function changeUserProfile(requestResponse) {
  const input = document.querySelector("input");
  if (input.style.display === "none") {
    input.style.display = "block";
  } else {
    input.style.display = "none";
  }
}

function deleteUserProfile(e, requestResponse) {
  e.parentNode.parentNode.removeChild(e.parentNode);
}
