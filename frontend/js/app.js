
document.getElementById("login").onclick = loginButtonFunction;
document.getElementById("register").onclick = redirectToRegisterPage;

function loginButtonFunction() {
  var usernameField = document.getElementById("username");
  var passwordField = document.getElementById("password");

  var usernameFieldValid = usernameField.checkValidity();
  var passwordFieldValid = passwordField.checkValidity();

  if (usernameField.value === "" || passwordField.value === "") {
    alert("All fields must be entered!");
    return;
  } else if (!usernameFieldValid || !passwordFieldValid) {
    alert("Field patterns not valid! Mouse over to check requirements!");
    return;
  }

  var requestData = 
    {
      username:usernameField.value,
      password:passwordField.value,
      task:"login"
    };
  
  var httpRequest = new XMLHttpRequest();

  httpRequest.onload = () => {
    if (httpRequest.status == 200) {
      var requestResponse = JSON.parse(httpRequest.response);
      sessionStorage.setItem('userID', requestResponse.id);
      sessionStorage.setItem('username', requestResponse.username);
      sessionStorage.setItem('firstname', requestResponse.firstname);
      sessionStorage.setItem('lastname', requestResponse.lastname);
      sessionStorage.setItem('userType', requestResponse.type);
      if (requestResponse.type == 'user') {
        window.location.assign("http://localhost/maliprojekat/testrepo/frontend/profile.html");
      } else if (requestResponse.type == 'admin') {
        window.location.assign("http://localhost/maliprojekat/testrepo/frontend/admin.html");
      }
      
    } else {
      var requestResponse = JSON.parse(httpRequest.response);
      alert(requestResponse.error);
    }
  }

  httpRequest.open("POST", "http://localhost/maliprojekat/testrepo/backend/controller.php", true);

  httpRequest.send(JSON.stringify(requestData));
}

function redirectToRegisterPage() {
  window.location.assign("http://localhost/maliprojekat/testrepo/frontend/register.html");
}
