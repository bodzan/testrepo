window.addEventListener("DOMContentLoaded", myFunction);

function myFunction() {
  let username = document.createElement("div");
  username.innerHTML = sessionStorage.getItem("username");
  let firstname = document.createElement("div");
  firstname.innerHTML = sessionStorage.getItem("firstname");
  let lastname = document.createElement("div");
  lastname.innerHTML = sessionStorage.getItem("lastname");

  let edit = document.createElement("button");
  edit.innerHTML = "Change password";

  let logout = document.createElement("button");
  logout.innerHTML = "Logout";

  let field = document.createElement("input");
  field.style.display = "none";
  // makeing an input field hidden

  let div = document.getElementById("content");
  div.appendChild(username);
  div.appendChild(firstname);
  div.appendChild(lastname);
  div.appendChild(field);
  div.appendChild(edit);
  div.appendChild(logout);

  edit.onclick = changePassword;
  logout.onclick = logoutButtonFunction;
}

function changePassword() {
  const input = document.querySelector("input");
  if (input.style.display === "none") {
    input.style.display = "block";
  } else {
    input.style.display = "none";
  }
}

function logoutButtonFunction() {
  sessionStorage.removeItem("userID");
  sessionStorage.removeItem("username");
  sessionStorage.removeItem("firstname");
  sessionStorage.removeItem("lastname");
  sessionStorage.removeItem("userType");
  window.location.assign(
    "http://localhost/maliprojekat/testrepo/frontend/index.html"
  );
}
