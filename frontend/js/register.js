document.getElementById("register").onclick = sendRegisterData;

function sendRegisterData() {
  var firstNameField = document.getElementById('firstname');
  var lastNameField = document.getElementById('lastname');
  var usernameField = document.getElementById('username');
  var passwordField = document.getElementById("newpassword");
  var passwordRepeatField = document.getElementById("newpasswordrepeat");


  var firstNameFieldValid = firstNameField.checkValidity();
  var lastNameFieldValid = lastNameField.checkValidity();
  var usernameFieldValid = usernameField.checkValidity();
  var passwordFieldValid = passwordField.checkValidity();
  var passwordRepeatFieldValid = passwordRepeatField.checkValidity();

  if (firstNameField.value === "" || lastNameField.value === "" || usernameField.value === "" 
  || passwordField.value === "" || passwordRepeatField.value === "") {
    alert("All fields must be entered!");
    return;
  } else if (!firstNameFieldValid || !lastNameFieldValid || !usernameFieldValid || !passwordFieldValid || !passwordRepeatFieldValid) {
    alert("Field patterns not valid! Mouse over to check requirements!");
    return;
  } else if (passwordField.value != passwordRepeatField.value) {
    alert(passwordField.value + " " + passwordRepeatField.value);
    return;
  }

  var requestData = {username:usernameField.value, password:passwordField.value, firstname:firstNameField.value, lastname:lastNameField.value, task:"register"};
  
  var httpRequest = new XMLHttpRequest();

  httpRequest.onload = () => {
    if (httpRequest.status == 200) {
      var requestResponse = JSON.parse(httpRequest.response);
      sessionStorage.setItem('userID', requestResponse.id);
      sessionStorage.setItem('username', requestResponse.username);
      sessionStorage.setItem('firstname', requestResponse.firstname);
      sessionStorage.setItem('lastname', requestResponse.lastname);
      sessionStorage.setItem('userType', requestResponse.type);
      if (requestResponse.type == 'user') {
        window.location.assign("http://localhost/maliprojekat/testrepo/frontend/profile.html");
      } else if (requestResponse.type == 'admin') {
        window.location.assign("http://localhost/maliprojekat/testrepo/frontend/admin.html");
      }
    } else {
      var requestResponse = JSON.parse(httpRequest.response);
      console.log(requestResponse.error);
    }
  }

  httpRequest.open("POST", "http://localhost/maliprojekat/testrepo/backend/controller.php", true);

  httpRequest.send(JSON.stringify(requestData));
}